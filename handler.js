const AWS = require('aws-sdk')
const ENDPOINT = process.env.Endpoint
const client = new AWS.ApiGatewayManagementApi({ endpoint : ENDPOINT });
const names = {};


const sendToOne = async(id,body) =>{
    try {
        await client.postToConnection({
            'ConnectionId' : id,
            'Data' : Buffer.from(JSON.stringify(body)),
        }).promise();
    } 
    catch (e) {
        console.log(e);
    }
    
}

const sendToAll = async(ids,body) =>{
    const all = ids.map(id => sendToOne(id,body))
    return Promise.all(all);
}

exports.handler = async(event) => {
    if (event.requestContext){
        const connectionId = event.requestContext.connectionId;
        const routeKey = event.requestContext.routeKey;
        let body = {};
        try {
            if (event.body){
                body = JSON.parse(event.body);
            }
        } 
        catch (e) {
            console.log(e);
        }
        switch (routeKey) {
            case '$connect':
                console.log("Connected")
                break;
            case '$disconnect':
                console.log("DisConnected")
                await sendToAll(Object.keys(names), {messgae: `${names[connectionId]} has left the chat`});
                delete names[connectionId]
                await sendToAll(Object.keys(names), {members : Object.values(names)});
                break;
            case '$default':
                // code
                break;
            case 'setName':
                names[connectionId] = body.name;
                await sendToAll(Object.keys(names),{messgae: `${names[connectionId]} has joined the chat`,setname:true });
                break;
            case 'sendPrivate':
                const to = Object.keys(names).find(key => names[key] === body.to);
                console.log(to)
                await sendToOne(to, {messgae: `${names[connectionId]}: ${body.message.message}`});
                break;
            case 'sendPublic':
                await sendToAll(Object.keys(names), {messgae: `${body.message.message}`, senderId:body.senderId});
                break;
            
            default:
                // code
        }
    }
    
    const response = {
        statusCode : 200,
        body : JSON.stringify('Hello from Lambda'),
    };
    return response;
    
};
